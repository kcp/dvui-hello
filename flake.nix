{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    zig.url = "github:mitchellh/zig-overlay";
  };
  outputs = { self, nixpkgs, flake-utils, zig }:
     flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            zig.overlays.default
          ];
        };
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.SDL2
            pkgs.SDL2.dev
            pkgs.pkg-config
            pkgs.zigpkgs.master
          ];
        };
      }
    );
}
