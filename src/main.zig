const std = @import("std");
const dvui = @import("dvui");
const Backend = @import("SDLBackend");

var gpa_instance = std.heap.GeneralPurposeAllocator(.{}){};
const gpa = gpa_instance.allocator();
const vsync = true;

pub fn main() !void {
    var backend = try Backend.init(.{
        .width = 480,
        .height = 360,
        .vsync = vsync,
        .title = "Hello, world!",
    });
    defer backend.deinit();

    var win = try dvui.Window.init(@src(), 0, gpa, backend.backend());
    defer win.deinit();
    win.theme = &dvui.Adwaita.light;

    main: while (true) {
        var nstime = win.beginWait(backend.hasEvent());
        try win.begin(nstime);
        backend.clear();

        const quit = try backend.addAllEvents(&win);
        if (quit or .exit == try frame()) break :main;

        const end_micros = try win.end(.{});
        backend.setCursor(win.cursorRequested());
        backend.renderPresent();

        const wait_event_micros = win.waitTime(end_micros, null);
        backend.waitEventTimeout(wait_event_micros);
    }
}

fn frame() !enum { ok, exit } {
    {
        var m = try dvui.menu(@src(), .vertical, .{
            .background = true,
            .expand = .horizontal,
        });
        defer m.deinit();

        const opts = dvui.Options{ .expand = .horizontal, .font_style = .heading };

        var tl = try dvui.textLayout(@src(), .{}, opts);
        try tl.addText("Hello, world! :3", .{});
        tl.deinit();

        tl = try dvui.textLayout(@src(), .{}, opts);
        try tl.addText("Testing dvui with a some menus and widgets.", .{});
        tl.deinit();
    }

    {
        var m = try dvui.menu(@src(), .horizontal, .{
            .background = true,
            .expand = .both,
        });
        defer m.deinit();

        if (try dvui.button(@src(), "Ok", .{ .color_style = .accent })) {
            return .exit;
        }

        if (try dvui.button(@src(), "Repository", .{})) {
            try dvui.openURL("https://git.disroot.org/kcp/dvui-hello");
        }
    }

    return .ok;
}
