const std = @import("std");
const dvui = @import("dvui");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const dvui_dep = b.dependency("dvui", .{
        .target = target,
        .optimize = optimize,
    });

    const exe = b.addExecutable(.{
        .name = "gui-hello",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    exe.addModule("dvui", dvui_dep.module("dvui"));
    exe.addModule("SDLBackend", dvui_dep.module("SDLBackend"));

    dvui.link_deps(exe, dvui_dep.builder);
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}

fn link_deps(b: *std.Build, exe: *std.Build.CompileStep) void {
    //const freetype_dep = b.dependency("freetype", .{
    //    .target = exe.target,
    //    .optimize = exe.optimize,
    //});
    //exe.linkLibrary(freetype_dep.artifact("freetype"));

    //const stbi_dep = b.dependency("stb_image", .{
    //    .target = exe.target,
    //    .optimize = exe.optimize,
    //});
    //exe.linkLibrary(stbi_dep.artifact("stb_image"));

    exe.linkLibC();

    if (exe.target.isWindows()) {
        const sdl_dep = b.dependency("sdl", .{
            .target = exe.target,
            .optimize = exe.optimize,
        });
        exe.linkLibrary(sdl_dep.artifact("SDL2"));

        exe.linkSystemLibrary("setupapi");
        exe.linkSystemLibrary("winmm");
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("imm32");
        exe.linkSystemLibrary("version");
        exe.linkSystemLibrary("oleaut32");
        exe.linkSystemLibrary("ole32");
    } else if (exe.target.isDarwin()) {
        exe.linkSystemLibrary("z");
        exe.linkSystemLibrary("bz2");
        exe.linkSystemLibrary("iconv");
        exe.linkFramework("AppKit");
        exe.linkFramework("AudioToolbox");
        exe.linkFramework("Carbon");
        exe.linkFramework("Cocoa");
        exe.linkFramework("CoreAudio");
        exe.linkFramework("CoreFoundation");
        exe.linkFramework("CoreGraphics");
        exe.linkFramework("CoreHaptics");
        exe.linkFramework("CoreVideo");
        exe.linkFramework("ForceFeedback");
        exe.linkFramework("GameController");
        exe.linkFramework("IOKit");
        exe.linkFramework("Metal");
        exe.linkSystemLibrary("SDL2");
    } else if (exe.target.isLinux()) {
        exe.linkSystemLibrary("SDL2");
    }
}
